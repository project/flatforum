<?php
  if (!_is_forum()) {
    include('node.tpl.php');
    return;
  }
  $curr_user = user_load(array('uid' => $userid));
  $sig = $curr_user->signature;
?>
<div class="comment forum-comment comment-<?php print $row_class; print $comment->new ? ' comment-new forum-comment-new' : ''; ?>">

  <div class="comment-left">
    <div class="author-name"><?php print $name ?></div>
    <?php print $picture ?>

    <?php if (module_exists('flatforum')): ?>
      <span class="author-posts">
        <?php print t('Posts:') . ' ' . $posts; ?><br />
      </span>
      <span class="author-regdate">
        <?php print t('Joined:') . ' ' . $joined; ?><br />
      </span>
    <?php endif ?>
  </div>

  <div class="comment-right">
    <div class="title"><?php print check_plain($comment->subject) ?></div>
    <?php if ($comment->new) : ?>
      <a id="new"></a>
      <span class="new"><?php print $new ?></span>
    <?php endif ?>
    <div class="content">
      <?php print $content ?>
      <?php if ($sig): ?>
        <div class="author-signature">--<br /><?php print check_markup($sig); ?></div>
      <?php endif ?>
      <br class="clear" />
      <div class="links"><?php print $submitted . ' ' . $links ?></div>
    </div>
  </div>

</div>
<br class="clear" />
