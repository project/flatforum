flatforum Template/Module README
---------------------------------
flatforum is not a full theme. It's a template that changes the look of forum so it resembles phpBB/vBulletin style flat forums. flatforum.module is needed for post count and join date information.

Requirements
-------------
Flatforum requires a theme based on the PHPTemplate engine.

Installation
-------------
1) Copy node-forum.tpl.php to your current theme folder.
2) Copy template.php to your current theme folder. If such a file already exists, you need to merge the two files.
3) Copy CSS code in flatforum.css to your current theme's stylesheet (usually named style.css), and customize it if necessary.
(Next is optional, only required for post counts and join date)
4) Copy flatforum's folder to your modules folder.
6) Go to administer > modules, and enable flatforum.

Customization
--------------
After installation, you may want to customize the look of your forum threads, to do so, edit the CSS code included in flatforum.css, mainly you would be interested in background colors and width values.

Tested with
------------
Bluemarine, PusButton, box_grey, and FriendsElectric (Should work with other PHPTemplate themes, but may need CSS modifications).
Under Firefox 1.x, IE 6, and Opera 8.x.

Known Issues
-------------
* PushButton's background image (background.png) appears over content.
  Workaround: Remove the background image from style.css.
* Due to their different structure, Flatforum doesn't work with chameleon/marvin.

How to Report Issues
---------------------
Before you report issues related to 3rd party themes, please consider tweaking CSS values found in flatforum.css, like width, padding and margin, Flatforum has been tested and found to be compatible with the themes that ship with Drupal, but unfortunately we can't support every contrib theme available with the same CSS code. Replies to support requests on contrib themes are not guaranteed.

To report issues, please include:
* a description of what's wrong (a link/screenshot would be helpful as well).
* Your browser version.
* Drupal and Flatforum versions.
* Your theme.

Credits
--------
* The code was originally written by Károly Négyesi.
* The CSS is Tibor Liktor's work.
(These phases were sponsered by http://ak47.dk, and RealROOT BVBA for http://www.zattevrienden.be/forum)
* Flatforum is currently maintained by Ayman Hourieh <drupal@aymanh.com>.
